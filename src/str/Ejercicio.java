package str;

public class Ejercicio {

	// 1. Hacer una función que tome un string y cuente la cantidad de veces
	// que aparece la letra 'e'.
	// Probarla en una función main de pruebas. (En un archivo aparte.)
	public static int cantidadDeE(String s) {
		int count = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.toLowerCase().charAt(i) == 'e') {
				count++;
			}
		}
		return count;
	}

	// 3. Hacer una función que devuelva true si ambos strings son iguales.
	public static boolean sonIguales(String s1, String s2) {
		if (s1.length() != s2.length()) {
			return false;
		}
		
		for (int i = 0; i < s1.length(); i++) {
			if (s1.charAt(i) != s2.charAt(i)) {
				return false;
			}
		}
		return true;
	}
	
	// 4. Hacer una función que tome un string y devuelva true si es "duódromo",
	// y false en caso contrario. Un string es "duódromo" si está compuesto
	// solamente por letras duplicadas contiguas.
	// Por ejemplo "llaammaa" es duodroma, "ssaabb" es duodroma.
	public static boolean esDuodromo(String s) {
		if (s.length() % 2 != 0) {
			return false;
		}
		
		for (int i = 0; i < s.length(); i += 2) {
			if (s.charAt(i) != s.charAt(i + 1)) {
				return false;
			}
		}
		
		return true;
	}
	
	// 5. Implementar la función que consulte si el string `prefijo`
	// coincide con los primeros caracteres de `s`.
	public static boolean esPrefijo(String prefijo, String s) {
		if (prefijo.length() > s.length()) {
			return false;
		}
		
		for (int i = 0; i < prefijo.length(); i++) {
			if (prefijo.charAt(i) != s.charAt(i)) {
				return false;
			}
		}
		
		return true;
	}
	
	// función auxiliar que sirve para los ejercicios 6 y 7
	public static String restoDesde(String s, int posicion) {
		String r = "";
		for (int i = posicion; i < s.length(); i++) {
			r += s.charAt(i);
		}
		return r;
	}
	
	public static void main(String[] args) {
		String nombre = "Nestor";
		System.out.println(cantidadDeE(nombre));
		System.out.println(sonIguales("Cristina", "CristinaKirchner"));
		System.out.println(esDuodromo("aallggoo"));
		System.out.println(esPrefijo("pre", "prepizza")); // true
		System.out.println(esPrefijo("prepizza", "prepizza")); // true
		System.out.println(esPrefijo("prepizza", "pre")); // false
	}

}
